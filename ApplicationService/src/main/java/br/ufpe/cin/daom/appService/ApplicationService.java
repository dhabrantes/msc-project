package br.ufpe.cin.daom.appService;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface ApplicationService {
	
	@WebMethod
	public String getServiceEndpoint(@WebParam(name="serviceId") String serviceId);

	@WebMethod
	public void activate(String variabilityName);

	@WebMethod
	public void deactivate(String variabilityName);
	
}
