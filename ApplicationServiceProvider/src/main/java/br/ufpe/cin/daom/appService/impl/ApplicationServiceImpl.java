package br.ufpe.cin.daom.appService.impl;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

import org.apache.felix.ipojo.annotations.Component;
import org.apache.felix.ipojo.annotations.Provides;
import org.apache.felix.ipojo.annotations.Requires;
import org.osgi.framework.BundleContext;
import org.osgi.framework.BundleException;

import br.ufpe.cin.daom.appService.ApplicationService;
import br.ufpe.cin.daom.serviceContext.exception.ServiceBundleException;
import br.ufpe.cin.daom.serviceContext.exception.ServiceNotFoundException;
import br.ufpe.cin.daom.serviceContext.exception.ServiceProviderNotFoundException;
import br.ufpe.cin.daom.serviceContext.service.ServiceContext;

@Component(propagation = true)
@Provides
@WebService(endpointInterface = "br.ufpe.cin.daom.appService.ApplicationService", serviceName = "ApplicationService")
public class ApplicationServiceImpl implements ApplicationService {

	private BundleContext bundleContext;

	public ApplicationServiceImpl(BundleContext bundleContext){
		this.bundleContext = bundleContext;
	}
	
	@Requires
	private ServiceContext serviceContext;
	
	@Override
	public String getServiceEndpoint(String serviceId){
		return this.serviceContext.getServiceEndpoint(serviceId);
	}

	@Override
	public void activate(String variabilityName) {
		try {
			this.serviceContext.activate(variabilityName);
		} catch (ServiceNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ServiceProviderNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (BundleException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ServiceBundleException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	@Override
	public void deactivate(String variabilityName) {
//		try {
//			this.serviceContext.deactivate(variabilityName);
//		} catch (ServiceNotFoundException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (ServiceProviderNotFoundException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (BundleException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (ServiceBundleException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		
	}

}
