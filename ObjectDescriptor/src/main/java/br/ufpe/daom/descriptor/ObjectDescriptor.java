package br.ufpe.daom.descriptor;

import java.io.File;
import java.io.InputStream;
import java.net.URL;

/**
 * 
 * @author  <a href="mailto:daom@cin.ufpe.br">Dhiego Abrantes de O. Martins</a>
 *
 * @param <T>
 */
public interface ObjectDescriptor<T> {

		/*
		 * 
		 */
		T getObjectDescriptors(String xml);
		
		/*
		 * 
		 */
		T getObjectDescriptors(File file);
		
		/*
		 * 
		 */
		T getObjectDescriptors(InputStream inputStream);
		
		/*
		 * 
		 */
		T getObjectDescriptors(URL url);
		
		/*
		 * 
		 */
		String toXML(T obj);
	}
