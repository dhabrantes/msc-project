package br.ufpe.cin.daom.serviceContext.exception;

/**
 * Exception é lançada caso um serviço alternativo não seja encontrado.
 * @author dhiego
 *
 */
public class ServiceBundleException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5614483750588466179L;

	public ServiceBundleException(String msg){
		super(msg);
	}
	
}
