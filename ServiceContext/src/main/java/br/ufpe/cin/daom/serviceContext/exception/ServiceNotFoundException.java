package br.ufpe.cin.daom.serviceContext.exception;

/**
 * Exception é lançada caso um serviço alternativo não seja encontrado.
 * @author dhiego
 *
 */
public class ServiceNotFoundException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1405490438284320429L;

	public ServiceNotFoundException(String msg){
		super(msg);
	}
	
}
