package br.ufpe.cin.daom.serviceContext.exception;

/**
 * Exception é lançada caso um provedor de serviço não seja encontrado.
 * @author dhiego
 *
 */
public class ServiceProviderNotFoundException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6050205668592895234L;

	public ServiceProviderNotFoundException(String msg){
		super(msg);
	}
	
}
