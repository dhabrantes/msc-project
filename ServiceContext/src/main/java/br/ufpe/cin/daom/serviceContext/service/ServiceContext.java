package br.ufpe.cin.daom.serviceContext.service;

import org.osgi.framework.BundleException;

import br.ufpe.cin.daom.serviceContext.exception.ServiceBundleException;
import br.ufpe.cin.daom.serviceContext.exception.ServiceNotFoundException;
import br.ufpe.cin.daom.serviceContext.exception.ServiceProviderNotFoundException;

/**
 * 
 * @author <a href="mailto:daom@cin.ufpe.br">Dhiego Abrantes de O. Martins</a>
 * 
 */
public interface ServiceContext {

	public String getServiceEndpoint(String serviceId);

	public void activate(String variabilityName) throws ServiceNotFoundException, ServiceProviderNotFoundException, BundleException,
			ServiceBundleException;

	public void deactivate(String variabilityName) throws ServiceNotFoundException, ServiceProviderNotFoundException, BundleException,
			ServiceBundleException;
}
