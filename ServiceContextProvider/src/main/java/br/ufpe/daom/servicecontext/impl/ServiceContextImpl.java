package br.ufpe.daom.servicecontext.impl;

import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBElement;

import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.BundleEvent;
import org.osgi.framework.BundleException;
import org.osgi.framework.BundleListener;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.ServiceReference;

import br.ufpe.cin.daom.serviceContext.exception.ServiceBundleException;
import br.ufpe.cin.daom.serviceContext.exception.ServiceNotFoundException;
import br.ufpe.cin.daom.serviceContext.exception.ServiceProviderNotFoundException;
import br.ufpe.cin.daom.serviceContext.service.ServiceContext;
import br.ufpe.cin.daom.serviceDescriptor.domain.AlternativeService;
import br.ufpe.cin.daom.serviceDescriptor.domain.Service;
import br.ufpe.cin.daom.serviceDescriptor.domain.Services;
import br.ufpe.cin.daom.serviceDescriptor.service.ServiceDescriptor;
import br.ufpe.cin.daom.variabilityDescriptor.domain.ServiceRef;
import br.ufpe.cin.daom.variabilityDescriptor.domain.Variabilities;
import br.ufpe.cin.daom.variabilityDescriptor.domain.Variability;
import br.ufpe.cin.daom.variabilityDescriptor.domain.Variant;
import br.ufpe.cin.daom.variabilityDescriptor.service.VariabilityDescriptor;
import br.ufpe.cin.daom.wsdlDescriptor.domain.TDefinitions;
import br.ufpe.cin.daom.wsdlDescriptor.service.WsdlDescriptor;
import br.ufpe.daom.servicecontext.util.BundleUtil;
import br.ufpe.daom.servicecontext.util.ClassUtil;

@SuppressWarnings({ "rawtypes", "unchecked" })
public class ServiceContextImpl implements ServiceContext, BundleListener{

	public final String SERVICES_DESCRIPTOR_PATH = "/Volumes/Dhiego/ESTUDOS/PROJETOS/MSc/mscproject/ServiceContextProvider/src/main/resources/services.xml";

	public final String VARIABILITIES_DESCRIPTOR_PATH = "/Volumes/Dhiego/ESTUDOS/PROJETOS/MSc/mscproject/ServiceContextProvider/src/main/resources/variabilities.xml";

	/**
	 * Pilha de serviços map<serviceName, serviceObj>
	 */
	private Map<String, Service> serviceStack;

	/**
	 * Pilha de Grupos de Variabilidades map<variabilityName, variabilityGroup>
	 */
	private Map<String, Variability> variabilityGroupStack;

	/**
	 * Objeto utilizado para tratar os descritores de serviços.
	 */
	private ServiceDescriptor serviceDescriptor;

	/**
	 * Objeto utilizado para tratar os descritores de variabilidades.
	 */
	private VariabilityDescriptor variabilityDescriptor;

	/**
	 * Objeto utilizado para tratar os descritores de wsdl.
	 */
	private WsdlDescriptor wsdlDescriptor;
	
	/**
	 * Objeto utilizado para manipular o conteiner OSGi.
	 */
	private BundleContext bundleContext;

	/*
	 * Constructor
	 */
	public ServiceContextImpl(BundleContext context) {
		this.bundleContext = context;
		this.serviceStack = new HashMap<String, Service>();
		this.variabilityGroupStack = new HashMap<String, Variability>();
	}

	/**
	 * Starting.
	 * 
	 * @throws Exception
	 */
	public void startingContext() throws Exception {
		this.bundleContext.addBundleListener(this);
		buildContext();
		this.getServiceEndpoint("hiService");
	}

	/**
	 * Stopping.
	 * 
	 * @throws BundleException
	 */
	public void stoppingContext() throws BundleException {
		System.out.println("Stopping ServiceContext");
	}

	public void bundleChanged(BundleEvent event) {
		String symbolicName = event.getBundle().getSymbolicName();
		int type = event.getType();
		System.out.println("[ DynxFW:BundleChanged: " + symbolicName + ", event.type: " + BundleUtil.getBundleEventState(type) + " ]");
		
		if( type == BundleEvent.INSTALLED ){
			try {
				this.rebuildContext();
			} catch (ServiceNotFoundException e) {
				e.printStackTrace();
			}
		}
		
	}
	
	/**
	 * Função responsável por montar o contexto de serviços.
	 * 
	 * @throws Exception
	 */
	private void buildContext() throws Exception {

		File servicesfile = new File(SERVICES_DESCRIPTOR_PATH);
		File variabilitiesfile = new File(VARIABILITIES_DESCRIPTOR_PATH);

		// Recuperando coleção de serviços descritos
		Services services = this.serviceDescriptor.getObjectDescriptors(servicesfile);

		// Recuperando grupo de variabilidades descritos
		Variabilities variabilities = this.variabilityDescriptor.getObjectDescriptors(variabilitiesfile);

		// Construindo pilha de serviços
		for (Service service : services.getService()) {
			this.serviceStack.put(service.getId(), service);
			System.out.println(service.getId());
		}

		// Resolvendo relacionamento entre serviços
		resolveServicesRelationships();

		// Construindo pilha de variabilidade
		for (Variability variability : variabilities.getVariability()) {
			this.variabilityGroupStack.put(variability.getId(), variability);
			System.out.println(variability.getId());
		}
	}
	
	/**
	 * Função responsável por atualizar o contexto de serviços.
	 * 
	 * @throws Exception
	 */
	private void rebuildContext() throws ServiceNotFoundException {

		this.variabilityGroupStack.clear();
		this.serviceStack.clear();
		
		File servicesfile = new File(SERVICES_DESCRIPTOR_PATH);
		File variabilitiesfile = new File(VARIABILITIES_DESCRIPTOR_PATH);

		// Recuperando coleção de serviços descritos
		Services services = this.serviceDescriptor.getObjectDescriptors(servicesfile);

		// Recuperando grupo de variabilidades descritos
		Variabilities variabilities = this.variabilityDescriptor.getObjectDescriptors(variabilitiesfile);

		// Construindo pilha de serviços
		for (Service service : services.getService()) {
			this.serviceStack.put(service.getId(), service);
			System.out.println(service.getId());
		}

		// Resolvendo relacionamento entre serviços
		resolveServicesRelationships();

		// Construindo pilha de grupos de variabilidade
		for (Variability variability : variabilities.getVariability()) {
			this.variabilityGroupStack.put(variability.getId(), variability);
			System.out.println(variability.getId());
		}
	}

	/**
	 * Função responsavel por resolver os relacionamentos entre serviços e
	 * serviços alternativos.
	 * 
	 * @throws Exception
	 */
	private void resolveServicesRelationships() throws ServiceNotFoundException {
		for (Service service : this.serviceStack.values()) {

			for (AlternativeService alternativeService : service.getAlternativeService()) {
				Service serviceRef = this.serviceStack.get(alternativeService.getRef());

				if (serviceRef != null)
					alternativeService.setService(serviceRef);
				else
					throw new ServiceNotFoundException("Service reference \'" + alternativeService.getRef() + "\' not found.");
			}

		}
	}

	/**
	 * *
	 * * * Métodos utilizados no mecanismo de descoberta de serviços
	 * * 
	 */
	
	/**
	 * 
	 * @param serviceId
	 * @return
	 * @throws InvalidSyntaxException
	 */
	public String getServiceEndpoint(String serviceId) {
		Service service = this.serviceStack.get(serviceId);

		if (service != null) {
			Bundle bundle = null;
			if(service.getServiceImpl() != null && !service.getServiceImpl().isEmpty()){
				//Buscando por Especificação e Implementação
				bundle = getSpecificBundle(service.getServiceSpec(), service.getServiceImpl());
			}
			if( bundle == null ){
				if( service.getAlternativeService().size() > 0 ){
					//Buscando por uma referencia de um serviço alternativo
					bundle = getAlternativeService(service);
				}
			}
			if( bundle == null ){
				//Buscando por qualquer serviço que implemente a mesma especificação
				bundle = getSpecificBundle( service.getServiceSpec() ); 
			}
			if(bundle != null )
				return retrieveEndpoint(bundle.getRegisteredServices());
		}
		return null;
	}
	
	/**
	 * Recupera especificamente o {@link Bundle} que atende a especificação 'classSpecification'
	 * e possui a implementação 'classImplementation'
	 * @param classSpecification Class Specification
	 * @param classImplementation Class Implementation
	 * @throws InvalidSyntaxException 
	 */
	private Bundle getSpecificBundle(String classSpecification, String classImplementation) {
		try{
			ServiceReference[] reference = this.bundleContext.getServiceReferences(classSpecification, null);
			
			if( reference != null ){
				List<ServiceReference> refs = new ArrayList<ServiceReference>(Arrays.asList(reference));
				
				for (ServiceReference serviceReference : refs) {
					Object service = this.bundleContext.getService( serviceReference );
					if (service.getClass().getName().equals(classImplementation)) {
						return serviceReference.getBundle();
					}
				}
			}
			return null;
		}catch(InvalidSyntaxException ex){
			ex.printStackTrace();
			return null;
		}
	}

	/**
	 * Recupera o primeiro bundle que implementa o classSpecification
	 * @param classSpecification
	 * @throws InvalidSyntaxException
	 * 
	 */
	private Bundle getSpecificBundle(String classSpecification) {
		try{
			ServiceReference[] references = this.bundleContext.getServiceReferences(classSpecification, null);
			
			if( references != null && references.length > 0 )
				return references[0].getBundle();
			else
				return null;
		}catch (InvalidSyntaxException e) {
			return null;
		}
	}
	
	/**
	 * Recupera o Bundle de um dos serviços alternativos, de acordo com a
	 * prioridade.
	 * 
	 * @param service
	 * @return
	 * @throws InvalidSyntaxException 
	 */
	private Bundle getAlternativeService(Service service) {
		// Estamos assumindo que está ordenado por prioridade
		for (int i = 0; i < service.getAlternativeService().size(); i++) {
			AlternativeService alternativeService = service.getAlternativeService().get(i);
			Bundle bundle = getSpecificBundle(alternativeService.getService().getServiceSpec(), alternativeService.getService()
					.getServiceImpl());
			if ( bundle != null ) {
				return bundle;
			}
		}
		return null;
	}
	
	/**
	 * Função responsável por recuperar um EndPoint para um Serviço disponível.
	 * O Endpoint é retornado no seguinte formato: <b>[url+?wsdl];[targetNamespace];[serviceName]</b>
	 * </br>
	 * Exemplo de valor retornado: "http://localhost:8080/helloService?wsdl<b>;</b>http://helloService.ws.daom.cin.ufpe.br<b>;</b>HelloService
	 * 
	 * @param registeredServices
	 * @return Endpoint
	 */
	private String retrieveEndpoint(ServiceReference[] registeredServices) {
		for (int i = 0; i < registeredServices.length; i++) {
			Object endPoint = registeredServices[i].getProperty("org.apache.cxf.ws.address");
			if (endPoint != null) {
				
				try {
					String value = endPoint.toString() + "?wsdl";
					
					if( value.contains("0.0.0.0")){
						value = value.replace("0.0.0.0", InetAddress.getLocalHost().getHostAddress());
					}else if( value.contains("localhost")){
						value = value.replace("localhost", InetAddress.getLocalHost().getHostAddress());
					}
					
					URL url = new URL(value);
					JAXBElement<TDefinitions> wsdl = this.wsdlDescriptor.getObjectDescriptors( url );
					
					value +=  ";" + wsdl.getValue().getTargetNamespace() + ";" + wsdl.getValue().getName();
					return value;
				} catch (MalformedURLException e) {
					e.printStackTrace();
				} catch (UnknownHostException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return null;
	}

	
	/**
	 * *
	 * * * Métodos utilizados no mecanismo de Reconfiguração de Serviços
	 * * 
	 */
	
	/**
	 * 
	 * @param variabilityName
	 * @throws ServiceNotFoundException 
	 * @throws ServiceProviderNotFoundException 
	 * @throws BundleException 
	 * @throws IOException 
	 * @throws ServiceBundleException 
	 */
	public void activate(String variabilityName) throws ServiceNotFoundException, ServiceProviderNotFoundException, BundleException, ServiceBundleException{
		Variant variant = findVariability(variabilityName);
		List<ServiceRef> serviceRefs = variant.getServiceRef();
		
		for (ServiceRef serviceRef : serviceRefs) {
			Service service = this.serviceStack.get(serviceRef.getRef());
			
			Bundle bundleSpec = findBundle(service.getServiceSpec());
			Bundle bundleProvider = findBundle(service.getServiceImpl());
			
			if( bundleSpec == null ){
				throw new ServiceNotFoundException("No service specification for \'" + service.getServiceSpec() + "\' was found.");
			}
			
			if( bundleProvider == null ){
				throw new ServiceProviderNotFoundException("No service provider matching \'" + service.getServiceImpl() + "\' was found.");
			}
			
			if( !(bundleSpec.getState() == Bundle.ACTIVE) ){
				System.out.println("SymbolicName: " + bundleSpec.getSymbolicName() + " | Current State: " + BundleUtil.getBundleState(bundleSpec));
				bundleSpec.start();
				System.out.println("SymbolicName: " + bundleSpec.getSymbolicName() + " | Current State: " + BundleUtil.getBundleState(bundleSpec));
			}else{
				System.out.println("Bundle '" + bundleSpec.getSymbolicName() + "' already active.");
			}
		
			if( !(bundleProvider.getState() == Bundle.ACTIVE) ){
				System.out.println("SymbolicName: " + bundleProvider.getSymbolicName() + " | Current State: " + BundleUtil.getBundleState(bundleProvider));
				bundleProvider.start();				
				System.out.println("SymbolicName: " + bundleProvider.getSymbolicName() + " | Current State: " + BundleUtil.getBundleState(bundleProvider));
			}else{
				System.out.println("Bundle '" + bundleProvider.getSymbolicName() + "' already active.");
			}
		}
	}
	
	/**
	 * 
	 * @param variabilityName
	 */
	public void deactivate(String variabilityName){
		Variability vGroup = this.variabilityGroupStack.get(variabilityName);
		
	}
	
	/**
	 * 
	 * @param variabilityName
	 * @return
	 */
	private Variant findVariability(String variantId){
		for (Variability variability : this.variabilityGroupStack.values()) {
			
			for (Variant variant : variability.getVariant()) {
				if( variant.getId().equals(variantId) )
					return variant;
			}
		}
		return null;
	}
	
	private Bundle findBundle(String fullClassName) throws ServiceBundleException{
		Bundle[] bundlesArray = this.bundleContext.getBundles();
		List<Bundle> bundles = new ArrayList<Bundle>(Arrays.asList(bundlesArray));
		
		//Removing System Bundle
		bundles.remove(0);

		String className = ClassUtil.getClassName(fullClassName);
		String packageName = ClassUtil.getPackageName(fullClassName);
		
		for (Bundle bundle : bundles) {
			Enumeration<URL> entries = bundle.findEntries("/", className + ".class", true);
			
			if( entries != null ){
				
				String bundleLocation = BundleUtil.getBundleLocation(bundle);
				
				if( bundleLocation.toUpperCase().contains("HTTP:") ){
					throw new ServiceBundleException("Http bundle location not supported.");
				}
				
				// TODO Inspecionar Manifest file em busca de exported-packages
//				JarFile jarFile = new JarFile( bundleLocation );
//				
//				Manifest m = jarFile.getManifest();
//				Map<String, Attributes> mapManifest = m.getEntries();
				
				while( entries.hasMoreElements() ){
					URL url = entries.nextElement();
					// TODO Alterar contains para uma checagem exata
					if( url.getPath().replace("/", ".").contains(packageName) ){
//						jarFile.close();
						return bundle;
					}
				}
//				jarFile.close();
			}			
		}
		return null;
	}
	
	
}
