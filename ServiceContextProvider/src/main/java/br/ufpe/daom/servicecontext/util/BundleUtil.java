package br.ufpe.daom.servicecontext.util;

import org.osgi.framework.Bundle;

public class BundleUtil{

	
	private BundleUtil(){
		
	}
	
	/**
	 * 
	 * @param bundle
	 * @return
	 */
	public static String getBundleLocation(Bundle bundle){
		return getBundleLocation(bundle.getLocation());
	}
	
	public static String getBundleLocation(String location){
		location = location.replace("file:", "");
		return location;
	}
	
	public static String getBundleState(Bundle bundle){
		return getBundleState(bundle.getState());
	}
	
	public static String getBundleState(int state){
		switch (state) {
		case 1:
			return "UNINSTALLED";
		case 2:
			return "INSTALLED";
		case 4:
			return "RESOLVED";
		case 8:
			return "STARTING";
		case 16:
			return "STOPPING";
		case 32:
			return "ACTIVE";
		default:
			return "UNKNOWN";
		}
	}
	
	public static String getBundleEventState(int state){
		switch (state) {
		case 1:
			return "INSTALLED";
		case 2:
			return "STARTED";
		case 4:
			return "STOPPED";
		case 8:
			return "UPDATED";
		case 16:
			return "UNINSTALLED";
		case 32:
			return "RESOLVED";
		case 64:
			return "UNRESOLVED";
		case 128:
			return "STARTING";
		case 256:
			return "STOPPING";
		case 512:
			return "LAZY_ACTIVATION";
		default:
			return "UNKNOWN";
		}
	}
	
}
