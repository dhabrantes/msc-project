package br.ufpe.daom.servicecontext.util;

public class ClassUtil{

	
	private ClassUtil(){
		
	}

	/**
	 * 
	 * @param fullClassName
	 * @return
	 */
	public static String getPackageName(String fullClassName){
		fullClassName = fullClassName.replace(".class", "").replace(".java", "").replace(".$class", "");
		String[] className = fullClassName.split("\\.");
		String packageName = "";

		for(int i = 0; i < className.length-1; i++){
			packageName += className[i];
			
			if( i < className.length-2 )
				packageName += ".";
		}
		return packageName;
	}
	
	/**
	 * 
	 * @param fullClassName
	 * @return
	 */
	public static String getClassName(String fullClassName){
		fullClassName = fullClassName.replace(".class", "").replace(".java", "");
		String[] fullClassNameSplitted = fullClassName.split("\\.");
		String className = "";

		if( fullClassNameSplitted.length > 0 ){
			className = fullClassNameSplitted[ fullClassNameSplitted.length-1 ];
		}
		return className;
	}
	
}
