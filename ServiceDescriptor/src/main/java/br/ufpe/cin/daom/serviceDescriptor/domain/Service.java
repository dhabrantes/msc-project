package br.ufpe.cin.daom.serviceDescriptor.domain;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * 
 * @author  <a href="mailto:daom@cin.ufpe.br">Dhiego Abrantes de O. Martins</a>
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "serviceSpec",
    "alternativeService"
})
@XmlRootElement(name = "service")
public class Service {

    @XmlElement(name = "service-spec", required = true)
    protected String serviceSpec;
    @XmlElement(name = "alternative-service")
    protected List<AlternativeService> alternativeService;
    @XmlAttribute
    protected String id;
    @XmlAttribute(name = "service-impl")
    protected String serviceImpl;

    /**
     * Gets the value of the serviceSpec property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceSpec() {
        return serviceSpec;
    }

    /**
     * Sets the value of the serviceSpec property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceSpec(String value) {
        this.serviceSpec = value;
    }

    /**
     * Gets the value of the alternativeService property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the alternativeService property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAlternativeService().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AlternativeService }
     * 
     * 
     */
    public List<AlternativeService> getAlternativeService() {
        if (alternativeService == null) {
            alternativeService = new ArrayList<AlternativeService>();
        }
        return this.alternativeService;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Gets the value of the serviceImpl property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceImpl() {
        return serviceImpl;
    }

    /**
     * Sets the value of the serviceImpl property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceImpl(String value) {
        this.serviceImpl = value;
    }

}