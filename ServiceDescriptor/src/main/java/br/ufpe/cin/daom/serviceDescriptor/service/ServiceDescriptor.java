package br.ufpe.cin.daom.serviceDescriptor.service;

import br.ufpe.cin.daom.serviceDescriptor.domain.Services;
import br.ufpe.daom.descriptor.ObjectDescriptor;

/**
 * 
 * @author  <a href="mailto:daom@cin.ufpe.br">Dhiego Abrantes de O. Martins</a>
 *
 */

public interface ServiceDescriptor extends ObjectDescriptor<Services> {

}
