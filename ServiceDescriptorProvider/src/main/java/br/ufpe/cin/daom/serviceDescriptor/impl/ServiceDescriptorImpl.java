package br.ufpe.cin.daom.serviceDescriptor.impl;

import java.io.File;
import java.io.InputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.net.URL;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import br.ufpe.cin.daom.serviceDescriptor.domain.Services;
import br.ufpe.cin.daom.serviceDescriptor.service.ServiceDescriptor;

/**
 * @author  <a href="mailto:daom@cin.ufpe.br">Dhiego Abrantes de O. Martins</a>
 */

public class ServiceDescriptorImpl implements ServiceDescriptor {

	private JAXBContext jaxbContext;

	@Override
	public Services getObjectDescriptors(String xml) {
		try {
			if (jaxbContext == null) {
				this.jaxbContext = JAXBContext.newInstance(new Class[] { Services.class });
			}

			StringReader stringReader = new StringReader(xml);

			Unmarshaller um = jaxbContext.createUnmarshaller();
			Services services = (Services) um.unmarshal(stringReader);

			return services;
		} catch (JAXBException e) {
			e.printStackTrace();
			return null;
		}

	}

	@Override
	public Services getObjectDescriptors(File file) {
		try {
			if (jaxbContext == null) {
				this.jaxbContext = JAXBContext.newInstance(new Class[] { Services.class });
			}

			Unmarshaller um = jaxbContext.createUnmarshaller();
			Services services = (Services) um.unmarshal(file);

			return services;
		} catch (JAXBException e) {
			e.printStackTrace();
			return null;
		}

	}

	@Override
	public Services getObjectDescriptors(InputStream inputStream) {
		try {
			if (jaxbContext == null) {
				this.jaxbContext = JAXBContext.newInstance(new Class[] { Services.class });
			}

			Unmarshaller um = jaxbContext.createUnmarshaller();
			Services services = (Services) um.unmarshal(inputStream);

			return services;
		} catch (JAXBException e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public String toXML(Services obj) {
		try {
			if (jaxbContext == null) {
				this.jaxbContext = JAXBContext.newInstance(new Class[] { Services.class });
			}
			StringWriter writer = new StringWriter();
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			jaxbMarshaller.marshal(obj, writer);

			String xml = writer.toString();

			return xml;
		} catch (JAXBException e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public Services getObjectDescriptors(URL url) {
		try {
			if (jaxbContext == null) {
				this.jaxbContext = JAXBContext.newInstance(new Class[] { Services.class });
			}

			Unmarshaller um = jaxbContext.createUnmarshaller();
			Services services = (Services) um.unmarshal(url);

			return services;
		} catch (JAXBException e) {
			e.printStackTrace();
			return null;
		}
	}

}
