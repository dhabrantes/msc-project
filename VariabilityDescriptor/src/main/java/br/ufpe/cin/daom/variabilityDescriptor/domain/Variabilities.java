package br.ufpe.cin.daom.variabilityDescriptor.domain;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * @author  <a href="mailto:daom@cin.ufpe.br">Dhiego Abrantes de O. Martins</a>
 * 
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}variability-group" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "variability"
})
@XmlRootElement(name = "variabilities")
public class Variabilities {

    @XmlElement(name = "variability", required = true)
    protected List<Variability> variability;

    /**
     * Gets the value of the variabilityGroup property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the variability property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getVariability().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Variability }
     * 
     * 
     */
    public List<Variability> getVariability() {
        if (variability == null) {
            variability = new ArrayList<Variability>();
        }
        return this.variability;
    }

}
