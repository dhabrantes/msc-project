package br.ufpe.cin.daom.variabilityDescriptor.service;

import br.ufpe.cin.daom.variabilityDescriptor.domain.Variabilities;
import br.ufpe.daom.descriptor.ObjectDescriptor;

public interface VariabilityDescriptor extends ObjectDescriptor<Variabilities>{

}
