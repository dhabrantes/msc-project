package br.ufpe.cin.daom.variabilityDescriptor.impl;

import br.ufpe.cin.daom.variabilityDescriptor.domain.Variabilities;

public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String xml_var = "";
		xml_var += "<variabilities>";
			xml_var += "<variability id=\"accessMode\">";
				xml_var += "<variant id=\"wifiMode\">";
					xml_var += "<service-ref ref=\"mainService\" />";
					xml_var += "<service-ref ref=\"hiService\" />";
				xml_var += "</variant>";
		
				xml_var += "<variant id=\"bluetoothMode\">";
					xml_var += "<service-ref ref=\"whatsUpService\" />";
					xml_var += "<service-ref ref=\"hiService\" />";
				xml_var += "</variant>";
				
				xml_var += "<variant id=\"infraredMode\">";
					xml_var += "<service-ref ref=\"irreplaceableService\"/>";
					xml_var += "<service-ref ref=\"irreplaceableService\"/>";
				xml_var += "</variant>";
			xml_var += "</variability>";
		xml_var += "</variabilities>";
		
		VariabilityDescriptorImpl desc = new VariabilityDescriptorImpl();
		
		
		
		Variabilities v = desc.getObjectDescriptors(xml_var);
		
		System.out.println(desc.toXML(v));

	}

}
