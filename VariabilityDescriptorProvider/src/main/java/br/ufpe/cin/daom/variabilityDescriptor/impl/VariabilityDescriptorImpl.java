package br.ufpe.cin.daom.variabilityDescriptor.impl;

import java.io.File;
import java.io.InputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.net.URL;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import br.ufpe.cin.daom.variabilityDescriptor.domain.Variabilities;
import br.ufpe.cin.daom.variabilityDescriptor.service.VariabilityDescriptor;

/**
 * @author  <a href="mailto:daom@cin.ufpe.br">Dhiego Abrantes de O. Martins</a>
 */

public class VariabilityDescriptorImpl implements VariabilityDescriptor {

	private JAXBContext jaxbContext;
	
	@Override
	public Variabilities getObjectDescriptors(String xml) {
		try {
			if (jaxbContext == null) {
				this.jaxbContext = JAXBContext.newInstance(new Class[] { Variabilities.class });
			}

			StringReader stringReader = new StringReader(xml);

			Unmarshaller um = jaxbContext.createUnmarshaller();
			Variabilities variabilities = (Variabilities) um.unmarshal(stringReader);

			return variabilities;
		} catch (JAXBException e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public Variabilities getObjectDescriptors(File file) {
		try {
			if (jaxbContext == null) {
				this.jaxbContext = JAXBContext.newInstance(new Class[] { Variabilities.class });
			}

			Unmarshaller um = jaxbContext.createUnmarshaller();
			Variabilities variabilities = (Variabilities) um.unmarshal(file);

			return variabilities;
		} catch (JAXBException e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public Variabilities getObjectDescriptors(InputStream inputStream) {
		try {
			if (jaxbContext == null) {
				this.jaxbContext = JAXBContext.newInstance(new Class[] { Variabilities.class });
			}

			Unmarshaller um = jaxbContext.createUnmarshaller();
			Variabilities variabilities = (Variabilities) um.unmarshal(inputStream);

			return variabilities;
		} catch (JAXBException e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public String toXML(Variabilities obj) {
		try {
			if (jaxbContext == null) {
				this.jaxbContext = JAXBContext.newInstance(new Class[] { Variabilities.class });
			}
			StringWriter writer = new StringWriter();
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			jaxbMarshaller.marshal(obj, writer);

			String xml = writer.toString();

			return xml;
		} catch (JAXBException e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public Variabilities getObjectDescriptors(URL url) {
		try {
			if (jaxbContext == null) {
				this.jaxbContext = JAXBContext.newInstance(new Class[] { Variabilities.class });
			}

			Unmarshaller um = jaxbContext.createUnmarshaller();
			Variabilities variabilities = (Variabilities) um.unmarshal(url);

			return variabilities;
		} catch (JAXBException e) {
			e.printStackTrace();
			return null;
		}
	}

}
