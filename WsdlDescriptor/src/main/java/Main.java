import java.io.File;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import br.ufpe.cin.daom.wsdlDescriptor.domain.TDefinitions;
import br.ufpe.cin.daom.wsdlDescriptor.domain.TDocumentation;
import br.ufpe.cin.daom.wsdlDescriptor.domain.TDocumented;


public class Main {

	private static JAXBContext jaxbContext;

	public static JAXBElement getObjectDescriptors(File file) {
		try {
			if (jaxbContext == null) {
				jaxbContext = JAXBContext.newInstance(new Class[] { TDefinitions.class });
			}

//			StringReader stringReader = new StringReader(xml);

			Unmarshaller um = jaxbContext.createUnmarshaller();
			JAXBElement<TDefinitions> defs = (JAXBElement<TDefinitions>) um.unmarshal(file);
			Object o = um.unmarshal(file);
			JAXBElement services = (JAXBElement) um.unmarshal(file);

			return services;
		} catch (JAXBException e) {
			e.printStackTrace();
			return null;
		}

	}
	
	public static void main(String[] args) {
		File file = new File("/Volumes/Dhiego/ESTUDOS/PROJETOS/MSc/WsdlDescriptor/src/main/resources/servicewsdl.xml");
		JAXBElement s2 = getObjectDescriptors(file);
	}
	
}
