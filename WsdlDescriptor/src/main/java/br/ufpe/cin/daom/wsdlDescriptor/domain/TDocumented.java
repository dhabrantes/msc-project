//
// Este arquivo foi gerado pela Arquitetura JavaTM para Implementa\u00e7\u00e3o de Refer\u00eancia (JAXB) de Bind XML, v2.2.5-2 
// Consulte <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas as modifica\u00e7\u00f5es neste arquivo ser\u00e3o perdidas ap\u00f3s a recompila\u00e7\u00e3o do esquema de origem. 
// Gerado em: PM.05.21 \u00e0s 10:13:05 PM BRT 
//


package br.ufpe.cin.daom.wsdlDescriptor.domain;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 * 				This type is extended by component types to allow them to be documented
 * 			
 * 
 * <p>Classe Java de tDocumented complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conte\u00fado esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="tDocumented">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="documentation" type="{http://schemas.xmlsoap.org/wsdl/}tDocumentation" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tDocumented", propOrder = {
    "documentation"
})
@XmlSeeAlso({
    TExtensibleAttributesDocumented.class,
    TExtensibleDocumented.class
})
public class TDocumented {

    protected TDocumentation documentation;

    /**
     * Obt\u00e9m o valor da propriedade documentation.
     * 
     * @return
     *     possible object is
     *     {@link TDocumentation }
     *     
     */
    public TDocumentation getDocumentation() {
        return documentation;
    }

    /**
     * Define o valor da propriedade documentation.
     * 
     * @param value
     *     allowed object is
     *     {@link TDocumentation }
     *     
     */
    public void setDocumentation(TDocumentation value) {
        this.documentation = value;
    }

}
