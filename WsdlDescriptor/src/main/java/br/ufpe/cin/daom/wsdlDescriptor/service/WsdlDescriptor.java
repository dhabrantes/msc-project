package br.ufpe.cin.daom.wsdlDescriptor.service;

import java.net.URL;

import javax.xml.bind.JAXBElement;

import br.ufpe.cin.daom.wsdlDescriptor.domain.TDefinitions;
import br.ufpe.daom.descriptor.ObjectDescriptor;

/**
 * 
 * @author  <a href="mailto:daom@cin.ufpe.br">Dhiego Abrantes de O. Martins</a>
 *
 */

public interface WsdlDescriptor extends ObjectDescriptor<JAXBElement<TDefinitions>> {

	
	
}
