package br.ufpe.cin.daom.wsdlDescriptor.impl;

import java.io.File;
import java.io.InputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.net.URL;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import br.ufpe.cin.daom.wsdlDescriptor.domain.TDefinitions;
import br.ufpe.cin.daom.wsdlDescriptor.service.WsdlDescriptor;

/**
 * @author  <a href="mailto:daom@cin.ufpe.br">Dhiego Abrantes de O. Martins</a>
 */

public class WsdlDescriptorImpl implements WsdlDescriptor {

	private JAXBContext jaxbContext;

	@Override
	public JAXBElement<TDefinitions> getObjectDescriptors(String xml) {
		try {
			if (jaxbContext == null) {
				this.jaxbContext = JAXBContext.newInstance(new Class[] { TDefinitions.class });
			}

			StringReader stringReader = new StringReader(xml);

			Unmarshaller um = jaxbContext.createUnmarshaller();
			JAXBElement<TDefinitions> wsdl = (JAXBElement<TDefinitions>) um.unmarshal(stringReader);

			return wsdl;
		} catch (JAXBException e) {
			e.printStackTrace();
			return null;
		}

	}

	@Override
	public JAXBElement<TDefinitions> getObjectDescriptors(File file) {
		try {
			if (jaxbContext == null) {
				this.jaxbContext = JAXBContext.newInstance(new Class[] { TDefinitions.class });
			}

			Unmarshaller um = jaxbContext.createUnmarshaller();
			JAXBElement<TDefinitions> wsdl = (JAXBElement<TDefinitions>) um.unmarshal(file);

			return wsdl;
		} catch (JAXBException e) {
			e.printStackTrace();
			return null;
		}

	}

	@Override
	public JAXBElement<TDefinitions> getObjectDescriptors(InputStream inputStream) {
		try {
			if (jaxbContext == null) {
				this.jaxbContext = JAXBContext.newInstance(new Class[] { TDefinitions.class });
			}

			Unmarshaller um = jaxbContext.createUnmarshaller();
			JAXBElement<TDefinitions> wsdl = (JAXBElement<TDefinitions>) um.unmarshal(inputStream);

			return wsdl;
		} catch (JAXBException e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public String toXML(JAXBElement<TDefinitions> obj) {
		try {
			if (jaxbContext == null) {
				this.jaxbContext = JAXBContext.newInstance(new Class[] { TDefinitions.class });
			}
			StringWriter writer = new StringWriter();
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			jaxbMarshaller.marshal(obj, writer);

			String xml = writer.toString();

			return xml;
		} catch (JAXBException e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public JAXBElement<TDefinitions> getObjectDescriptors(URL url) {
		try {
			if (jaxbContext == null) {
				this.jaxbContext = JAXBContext.newInstance(new Class[] { TDefinitions.class });
			}

			Unmarshaller um = jaxbContext.createUnmarshaller();
			JAXBElement<TDefinitions> wsdl = (JAXBElement<TDefinitions>) um.unmarshal(url);

			return wsdl;
		} catch (JAXBException e) {
			e.printStackTrace();
			return null;
		}
	}

}
